import os

IGNORE = ['__init__.py', '__pycache__']

print('\n------PEP8------')
for file in os.listdir('./src'):
    if file not in IGNORE:
        error = os.system('pycodestyle src/{}'.format(file))
        if not error:
            print('{}......OK'.format(file))
        else:
            print('{}.....FAILED.....'.format(file))
print('')
