#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import os
from xml.sax import make_parser
# from src import SmallSMILHandler
karaokePath = os.path.abspath("src/karaoke.py")
print(karaokePath)
from src import karaoke
# from src.smallsmilhandler import sHandler
import json
from urllib.request import urlretrieve
import unittest


class Test_KaraokeLocal(unittest.TestCase):

    maxDiff = None

    @classmethod
    def setUpClass(cls):        
        fich = os.path.abspath("karaoke.smil")
        cls.test_karaokeLocal = karaoke.KaraokeLocal(fich)
        cls.tags = {
            'root-layout': ['width', 'height', 'background-color'],
            'region': ['id', 'top', 'bottom', 'left', 'right'],
            'img': ['src', 'region', 'begin', 'dur'],
            'audio': ['src', 'begin', 'dur'],
            'textstream': ['src', 'region']}

    def test_str(self):
        mock = "root-layout\twidth='248'\theight='300'\tbackground-color='blue'\nregion\tid='a'\ttop='20'\tleft='64'\nregion\tid='b'\ttop='120'\tleft='20'\nregion\tid='text_area'\ttop='100'\tleft='20'\nimg\tsrc='hello.jpg'\tregion='a'\tbegin='2s'\tdur='36s'\nimg\tsrc='earthrise.jpg'\tregion='b'\tbegin='12s'\naudio\tsrc='hello.wav'\tbegin='1s'\ntextstream\tsrc='letra.rt'\tregion='text_area'\naudio\tsrc='cancion.ogg'\tbegin='4s'"

        self.assertEqual(self.test_karaokeLocal.__str__(), mock)

    def test_to_json(self):
        # if there is no a Json file
        name_smil = 'name'
        self.test_karaokeLocal.to_json(name_smil)
        # if there is a Json file
        name_json = 'name.json'
        self.test_karaokeLocal.to_json(name_smil, name_json)

    def test_do_local(self):
        # In this test it will get the tags from the setUp
        self.test_karaokeLocal.do_local()

if __name__ == '__main__':
    unittest.main()
