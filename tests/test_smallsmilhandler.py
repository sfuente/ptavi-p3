#!/usr/bin/python3
# -*- coding: utf-8 -*-

from xml.sax import make_parser
from xml.sax.handler import ContentHandler

from src import smallsmilhandler
import unittest


class Test_SmallSMILHandler(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        cls.list = []
        cls.tags = {
            'root-layout': ['width', 'height', 'background-color'],
            'region': ['id', 'top', 'bottom', 'left', 'right'],
            'img': ['src', 'region', 'begin', 'dur'],
            'audio': ['src', 'begin', 'dur'],
            'textstream': ['src', 'region']}
        cls.test_smallsmillhandler = smallsmilhandler.SmallSMILHandler()

    def test_startElement(self):
        """should open correctly the tags"""
        # Case where name is not in tags
        name = 'name'
        attrs = 'attrs'
        self.test_smallsmillhandler.startElement(name, attrs)
        # Case where name is in tags
        name = 'region'
        attrs = {'id': '0', 'top': '0', 'bottom': '0', 'left': '0', 'right': '0'}
        self.test_smallsmillhandler.startElement(name, attrs)

    def test_get_tags(self):
        """Should return a list of tags"""
        result = self.test_smallsmillhandler.get_tags()
        expected_result = []
        self.assertEqual(result, expected_result)

if __name__ == "__main__":
    unittest.main()
